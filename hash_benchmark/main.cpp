#include <iostream>
#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>
#include <openssl/rand.h>
#include <chrono>
#include <cmath>

int LAMBDA;
int K;
int REPEAT;

BIGNUM* ONE;

unsigned char *in_mem;
unsigned char *out_mem;

// std::cout << "x = " << BN_bn2dec(x) << std::endl;



void multiple_sha256_memset_before(BIGNUM *r, BIGNUM *x){
    for(int i = 0; i<REPEAT; i++)
    {
        BN_bn2bin(x, in_mem);
        unsigned int size_written = 0;

        EVP_Digest(in_mem, BN_num_bytes(x), out_mem, &size_written, EVP_sha256(), NULL);
        BN_bin2bn(out_mem, size_written, r);
    }
    std::clog << BN_bn2dec(r) << std::endl;
}


void multiple_sha256_memset_after(BIGNUM *r, BIGNUM *x){
    for(int i = 0; i<REPEAT; i++)
    {
        BN_bn2bin(x, in_mem);
        unsigned int size_written = 0;

        EVP_Digest(in_mem, BN_num_bytes(x), out_mem, &size_written, EVP_sha256(), NULL);
        BN_bin2bn(out_mem, size_written, r);
    }
    std::clog << BN_bn2dec(r) << std::endl;
}


void multiple_sha256_complete_memset_after(BIGNUM *r, BIGNUM *x){
    memset(in_mem, 0, K/8);
    memset(out_mem, 0, K/8);
    EVP_MD_CTX *mdctx;
    for(int i = 0; i<REPEAT; i++)
    {
        BN_bn2bin(x, in_mem);
        unsigned int size_written = 0;

        mdctx = EVP_MD_CTX_new();
        EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL);
        EVP_DigestUpdate(mdctx, in_mem, BN_num_bytes(x));
        EVP_DigestFinal_ex(mdctx, out_mem, &size_written);
        EVP_MD_CTX_free(mdctx);
        BN_bin2bn(out_mem, size_written, r);
    }
    std::clog << BN_bn2dec(r) << std::endl;
}


void multiple_shake256_complete_memset_after(BIGNUM *r, BIGNUM *x){
    memset(in_mem, 0, K/8);
    memset(out_mem, 0, K/8);
    EVP_MD_CTX *mdctx;
    for(int i = 0; i<REPEAT; i++)
    {
        BN_bn2bin(x, in_mem);
        unsigned int size_written = 0;

        mdctx = EVP_MD_CTX_new();
        EVP_DigestInit_ex(mdctx, EVP_shake256(), NULL);
        EVP_DigestUpdate(mdctx, in_mem, BN_num_bytes(x));
        EVP_DigestFinal_ex(mdctx, out_mem, &size_written);
        EVP_MD_CTX_free(mdctx);
        BN_bin2bn(out_mem, size_written, r);
    }
    std::clog << BN_bn2dec(r) << std::endl;
}

void multiple_blake256_complete_memset_after(BIGNUM *r, BIGNUM *x){
    memset(in_mem, 0, K/8);
    memset(out_mem, 0, K/8);
    EVP_MD_CTX *mdctx;
    for(int i = 0; i<REPEAT; i++)
    {
        BN_bn2bin(x, in_mem);
        unsigned int size_written = 0;

        mdctx = EVP_MD_CTX_new();
        EVP_DigestInit_ex(mdctx, EVP_blake2s256(), NULL);
        EVP_DigestUpdate(mdctx, in_mem, BN_num_bytes(x));
        EVP_DigestFinal_ex(mdctx, out_mem, &size_written);
        EVP_MD_CTX_free(mdctx);
        BN_bin2bn(out_mem, size_written, r);
    }
    std::clog << BN_bn2dec(r) << std::endl;
}


void incr_sha256(BIGNUM *r, BIGNUM *x){
    EVP_MD_CTX *main_mdctx = EVP_MD_CTX_new();
    EVP_MD_CTX *tmp_mdctx = EVP_MD_CTX_new();
    BN_bn2bin(x, in_mem);
    int n = BN_num_bytes(x);
    unsigned int size_written = 0;

    EVP_DigestInit_ex(main_mdctx, EVP_sha256(), NULL);
    EVP_DigestUpdate(main_mdctx, in_mem, n-1);

    unsigned short increment = in_mem[n-1] + (in_mem[n-2]<<8);

    for(int i = 0; i<REPEAT; i++)
    {
        in_mem[n-1] = increment&0xFF;
        in_mem[n-2] = (increment>>8)&0xFF;
        EVP_MD_CTX_copy(tmp_mdctx, main_mdctx);
        EVP_DigestUpdate(tmp_mdctx, in_mem+n-1, 1);
        EVP_DigestFinal_ex(tmp_mdctx, out_mem, &size_written);
        BN_bin2bn(out_mem, size_written, r);
        increment++;
    }
    EVP_MD_CTX_free(main_mdctx);
    EVP_MD_CTX_free(tmp_mdctx);
    std::cout << BN_bn2dec(r) << std::endl;
}


int main(int argc, char *argv[])
{
    LAMBDA = std::atoi(argv[1]);
    K = std::atoi(argv[2]);
    REPEAT = std::atoi(argv[3]);

    ONE = BN_new();
    BN_one(ONE);

    BIGNUM *x = BN_new();
    BIGNUM *r = BN_new();
    BN_rand(x, LAMBDA, NULL, NULL);

    in_mem = (unsigned char *) malloc(K/4);
    out_mem = (unsigned char *) malloc(K/4);

    auto start = std::chrono::steady_clock::now();
    multiple_sha256_memset_before(r, x);
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> time = end-start;
    std::cout << time.count() << std::endl;

    start = std::chrono::steady_clock::now();
    multiple_sha256_memset_after(r, x);
    end = std::chrono::steady_clock::now();
    time = end-start;
    std::cout << time.count() << std::endl;

    start = std::chrono::steady_clock::now();
    multiple_sha256_complete_memset_after(r, x);
    end = std::chrono::steady_clock::now();
    time = end-start;
    std::cout << time.count() << std::endl;

    start = std::chrono::steady_clock::now();
    multiple_shake256_complete_memset_after(r, x);
    end = std::chrono::steady_clock::now();
    time = end-start;
    std::cout << time.count() << std::endl;

    start = std::chrono::steady_clock::now();
    multiple_blake256_complete_memset_after(r, x);
    end = std::chrono::steady_clock::now();
    time = end-start;
    std::cout << time.count() << std::endl;


    start = std::chrono::steady_clock::now();
    incr_sha256(r, x);
    end = std::chrono::steady_clock::now();
    time = end-start;
    std::cout << time.count() << std::endl;
}