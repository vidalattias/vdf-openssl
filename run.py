import os

lambda_values = ['512', '1024', '2048', '4096']
k_values = ['64', '128', '256', '512']
repeat = 10
repeat_large = 3

for i in range(repeat_large):
    print("I=" + str(i))
    for lamda in lambda_values:
        print("\t" + lamda)
        for k in k_values:
            print("\t\t" + str(k))
            for r in range(repeat):
                command = 'src/wesolowski ' + lamda + ' ' + k + " 1 >> result/" + lamda + "_" + k + ".csv"
                a = os.system(command)
