#ifndef WESOLOWSKI_H
#define WESOLOWSKI_H

#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>


void evaluate(BIGNUM* pi, BIGNUM* l, BIGNUM* m, BIGNUM* tau, BIGNUM* N);
bool verify(BIGNUM* m, BIGNUM* tau, BIGNUM* pi, BIGNUM* l, BIGNUM* N);


#endif
