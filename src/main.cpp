#include <iostream>
#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>
#include <openssl/rand.h>
#include <chrono>
#include <cmath>
#include <cassert>

#include "wesolowski.hpp"

int LAMBDA;

#ifdef SECURITY_112
    int K = 128;
#endif
#ifdef SECURITY_128
    int K = 128;
#endif
#ifdef SECURITY_192
    int K = 192;
#endif
#ifdef SECURITY_256
    int K = 256;
#endif


BIGNUM *one_m = BN_new();
BIGNUM *one = BN_new();
BIGNUM *two = BN_new();
BIGNUM *tau_power = BN_new();
BIGNUM *y = BN_new();
BIGNUM *y_verif = BN_new();
BIGNUM *x = BN_new();
BIGNUM *x_verif = BN_new();
BIGNUM *pi = BN_new();
BIGNUM *l = BN_new();
BIGNUM *l_verif = BN_new();
BIGNUM *r = BN_new();
BIGNUM *tmp1 = BN_new();
BIGNUM *tmp2 = BN_new();
BN_CTX *ctx = BN_CTX_new();
BN_MONT_CTX* mctx = BN_MONT_CTX_new();

unsigned char *in_mem;
unsigned char *out_mem;
// std::cout << "x = " << BN_bn2dec(x) << std::endl;

int main(int argc, char *argv[])
{
    unsigned char t[1];

    LAMBDA = std::atoi(argv[1]);
    t[0] = std::atoi(argv[2]);

    in_mem = (unsigned char *) malloc(LAMBDA/4);
    out_mem = (unsigned char *) malloc(K/4);

    BIGNUM *p = BN_new();
    BIGNUM *q = BN_new();
    BIGNUM *N = BN_new();
    BIGNUM *m = BN_new();
    BIGNUM *tau = BN_new();

    BN_set_bit(one, 0);
    BN_set_bit(two, 1);
    BN_bin2bn(t, 1, tau);
    BN_exp(tau, two, tau, ctx);
    BN_exp(tau_power, two, tau, ctx);

    BN_generate_prime_ex(p, LAMBDA/2, false, NULL, NULL, NULL);
    BN_generate_prime_ex(q, LAMBDA/2, false, NULL, NULL, NULL);

    BN_mul(N, p, q, ctx);

    BN_rand_range(m, N);


    std::cout << "Starting eval" << std::endl;

    evaluate(pi, l, m, tau_power, N);


    //std::cout << "Starting verif" << std::endl;


    auto start = std::chrono::steady_clock::now();
    verify(m, tau_power, pi, l, N);
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> time = end-start;
    std::cout << 1000*time.count() << std::endl;


    EVP_cleanup();

    std::cout << "Fin" << std::endl;

    exit(0);
}
