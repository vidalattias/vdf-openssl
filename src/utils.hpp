#ifndef UTILS_H
#define UTILS_H

#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>


#include <iostream>


/**
 * Returns True if bits at index J of x and y are simultaneously 0, returns False otherwise
 * Used in the Lenstra multiexponentiation to alleviate the code reading.
 *
 * @param  x Exponent 1
 * @param  y Exponent 2
 * @param  j Index to test the bits
 * @return
 */
bool simultaneous_zero_bit(BIGNUM* x, BIGNUM* y, int j);

/**
 * [filter description]
 * @param  e [description]
 * @param  j [description]
 * @param  J [description]
 * @return
 */
int filter(BIGNUM* e, int i, int j);

void BN_mod_mul_montgomery_custom(BIGNUM* res, BIGNUM* x, BIGNUM* y, BN_MONT_CTX* mctx, BN_CTX* ctx, int w);

#endif
