#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "utils.hpp"

extern BN_CTX* ctx;
extern BN_MONT_CTX* mctx;
extern BIGNUM* one_m;
extern BIGNUM* one;
extern int count_lenstra[3];

void lenstra_precomputations_montgomery(BIGNUM** pc, int pow_w, BIGNUM* x_m, BIGNUM* y_m, BIGNUM* N, int w)
{
        for(int i = 0; i<pow_w; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {
                        pc[i * pow_w + j] = BN_new();
                        BN_copy(pc[i * pow_w + j], one_m);
                }
        }


        // Precomputation computations itself.
        BN_copy(pc[1 * pow_w + 0], x_m);
        BN_copy(pc[0 * pow_w + 1], y_m);

        
        BN_mod_mul_montgomery(pc[2 * pow_w + 0], x_m, x_m, mctx, ctx);



        BN_mod_mul_montgomery(pc[0 * pow_w + 2], y_m, y_m, mctx, ctx);

        int mid = pow_w/2;
        for(int i = 1; i < mid; i++)
        {
                BN_mod_mul_montgomery(pc[(2*i+1) * pow_w + 0], pc[(2*(i-1)+1) * pow_w + 0], pc[2 * pow_w + 0], mctx, ctx);
                

                BN_mod_mul_montgomery(pc[0 * pow_w + 2*i+1], pc[0 * pow_w + 2*(i-1)+1], pc[0 * pow_w + 2], mctx, ctx);
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < pow_w; j++)
                {

                        BN_mod_mul_montgomery(pc[j * pow_w + 2*i+1], pc[(j-1) * pow_w + 2*i+1], x_m, mctx, ctx);
                }
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < mid; j++)
                {
                        BN_mod_mul_montgomery(pc[(2*i+1) * pow_w + 2*j], pc[(2*i+1) * pow_w + 2*j-1],y_m, mctx, ctx);
                }
        }

        /*

           for(int i = 0; i<pow_w; i++)
           {
                for(int j = 0; j < pow_w; j++)
                {
                        std::cout << BN_bn2dec(pc[j*pow_w+i]) << "\t";
                }
                std::cout << std::endl;
           }

         */
}


/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */
void lenstra_exponentiation_montgomery(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w, BIGNUM** pc)
{
        int pow_w = pow(2, w);

        int A = 0;
        int B = 0;

        BN_copy(ret, one_m);
        int j = k-1;



        while(j>=0)
        {
                if(simultaneous_zero_bit(a, b, j))
                {
                        j--;
                        BN_mod_mul_montgomery(ret, ret, ret, mctx, ctx);
                }
                else
                {
                        int j_new = std::max(j-w, -1);

                        int J = j_new+ 1;

                        while(simultaneous_zero_bit(a,b, J) == true)
                        {
                                J++;
                        }


                        A = filter(a, j, J);
                        B = filter(b, j, J);

                        while(j>=J)
                        {
                                j--;
                                BN_mod_mul_montgomery(ret, ret, ret, mctx, ctx);
                        }


                        BN_mod_mul_montgomery(ret, ret, pc[A * pow_w + B], mctx, ctx);

                        while(j>j_new)
                        {
                                BN_mod_mul_montgomery(ret, ret, ret, mctx, ctx);

                                j--;

                        }

                }
        }
}


void lenstra_multiexp(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w)
{
        BN_MONT_CTX_set(mctx, N, ctx);
        int pow_w = pow(2, w);
        BIGNUM* pc[pow_w][pow_w];

        BIGNUM *x_m = BN_new();
        BIGNUM *y_m = BN_new();
        BIGNUM* ret_m = BN_new();

        BN_to_montgomery(x_m, x, mctx, ctx);
        BN_to_montgomery(y_m, y, mctx, ctx);


        BN_to_montgomery(one_m, one, mctx, ctx);

        lenstra_precomputations_montgomery((BIGNUM **) pc, pow_w, x_m, y_m, N, w);
        lenstra_exponentiation_montgomery(ret_m, x_m, y_m, a, b, N, k, w, (BIGNUM **) pc);

        BN_from_montgomery(ret, ret_m, mctx, ctx);
}
