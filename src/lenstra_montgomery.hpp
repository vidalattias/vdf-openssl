#ifndef LENSTRA_MONTGOMERY_H
#define LENSTRA_MONTGOMERY_H

#include <openssl/ssl.h> /* core library */
#include <openssl/bn.h>

/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */

void lenstra_precomputations_montgomery(BIGNUM** pc_m, int pow_w, BIGNUM* x_m, BIGNUM* y_m, BIGNUM* N, int w);

void lenstra_exponentiation_montgomery(BIGNUM* ret, BIGNUM* x_m, BIGNUM* y_m, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w, BIGNUM* *pc_m);

void lenstra_multiexp(BIGNUM* ret, BIGNUM* x, BIGNUM* y, BIGNUM* a, BIGNUM* b, BIGNUM* N, int k, int w);

#endif
